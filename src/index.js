import React, { Component } from 'react';
import { render} from 'react-dom';
import  StorePicker  from "./components/store-picker";
import App from './components/app';
import './css/style.css';
import { BrowserRouter, Match, Miss } from 'react-router';
import notFound from "./components/not-found";

const Root = () => (
    <BrowserRouter>
        <div>
            <Match exactly pattern='/' component={StorePicker} />
            <Match exactly pattern='/store/:storeId' component={App} />
            <Miss component={notFound}/>
        </div>

    </BrowserRouter>
        )

render(<Root />, document.querySelector('#main'));