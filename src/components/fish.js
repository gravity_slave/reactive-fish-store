import React, { Component } from 'react';
import {formatPrice} from "../helpers";

class Fish extends Component {
    constructor(props) {
        super(props);
    }
    render() {

        const  { image, name, price, description, status } = this.props.details;
        const isAvailable = status === 'available';
        const btnText = isAvailable ? 'Add to Order' : 'Sold Out';
        return (
            <li className='menu-fish'>
                <img src={image} alt={name} />
                <h3 className='fish-name'>
                    {name}
                    <span className="price">{formatPrice(price)}</span>
                    </h3>
                <p>{description}</p>
                <button onClick={ () => this.props.addToOrder(this.props.index)} disabled={!isAvailable}>{btnText}</button>
            </li>
        )
    }
}

export default Fish;