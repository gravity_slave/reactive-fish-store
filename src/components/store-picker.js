import React, { Component } from 'react';
import { getFunName } from "../helpers";


export default class StorePicker extends Component {
    // constructor(props) {
    //     super(props);
    //     this.goToStore = this.goToStore(e).bind(this);
    // }
    goToStore(e)  {
        e.preventDefault();
        console.log('You have changed the url');
        console.log(this.storeInput.value);
        this.context.router.transitionTo(`/store/${this.storeInput.value}`)

    }

    render() {
        return (
            <form className="store-selector" onSubmit={ e => this.goToStore(e) }>
                <h2>Please enter a store</h2>
                <input type="text" required placeholder='Store Name'
                       ref={ input => this.storeInput = input }
                defaultValue={getFunName()} />
                <button type='submit'>Visit Store</button>
            </form>
        )
    }
}

StorePicker.contextTypes = {
  router: React.PropTypes.object
};

