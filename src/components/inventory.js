import React, { Component } from 'react';
import AddFish from "./add-fish";


class Inventory extends Component {
    constructor(props) {
        super(props);
        this.renderInventory = this.renderInventory.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e, key) {
        const fish = this.props.fishes[key];
        const updatedFish = {
            ...fish,
            [e.target.name]: [e.target.value]
        };
        this.props.updateFish(key, updatedFish);
    }



    renderInventory(key, i) {
        const fish = this.props.fishes[key];
        return (
            <div key={i} className='fish-edit'>
                <input type="text"
                       value={fish.name}
                       name='name'
                       placeholder='Fish Name'
                       onChange={ e => this.handleChange(e, key)}

                />
                <input type="text" value={fish.price} name='price' placeholder='Fish Price' onChange={ e => this.handleChange(e, key)} />
                <select name='status' value={fish.status} onChange={ e => this.handleChange(e, key) } >
                    <option value="available">Fresh!</option>
                    <option value="unavailable">Sold Out!</option>
                </select>
                <textarea  placeholder='Fish Desc' value={fish.desc} onChange={ e => this.handleChange(e, key)} >

                </textarea>
                <input value={fish.image} type="text" placeholder='Fish Image' onChange={ e => this.handleChange(e, key)} />
                <button onClick={ () => this.props.removeFish(key) }>Remove FIsh</button>
                </div>
        )
    }


    render() {
        return (
            <div>
                <h2>Inventory</h2>
                {Object
                    .keys(this.props.fishes)
                    .map(this.renderInventory)}
                <AddFish addFish={this.props.addFish} />
                <button onClick={this.props.loadSamples}>Load Sample Fishes</button>
            </div>

        );
    }
}

export default Inventory;
