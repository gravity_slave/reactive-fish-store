import React, { Component } from 'react';


class AddFish extends Component {

    createFish(e) {
        e.preventDefault();
        console.log('gonna make some fish');
        const fish = {
            name: this.name.value,
            price: this.price.value,
            status:  this.status.value,
            desc: this.desc.value,
            image: this.image.value
        };
        console.log(fish);
        this.props.addFish(fish);
        this.fishForm.reset();
    }
    render() {
        return (
            <form className='fish-edit'
                   ref={ f => this.fishForm = f}
                  onSubmit={ e => this.createFish(e) }>
                <input ref={ input => this.name = input } type="text" placeholder='Fish Name'/>
                <input ref={input => this.price = input } type="text" placeholder='Fish Price'/>
                <select ref={ select => this.status = select }>
                    <option value="available">Fresh!</option>
                    <option value="unavailable">Sold Out!</option>
                </select>
                <textarea  placeholder='Fish Desc' ref={ text => this.desc = text }>

                </textarea>
                <input ref={ input => this.image = input } type="text" placeholder='Fish Image'/>
                <button type='submit'>Add FIsh</button>
            </form>

        );
    }
}

export default AddFish;
